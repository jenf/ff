#Activate Drone Mode

###Step 1
Get coordinates to aim at **target**

###Step 2
Fire shot if available

###Step 3
Is angle *near* and timer *elapsed*?  
	 If yes, shooting available
	 If no, shooting not allowed

###Step 4
Record previous position

###Step 5
Follow path when not evading enemies

###Step 6 
Get list of **points** to go through to reach target

###Step 7
Fire shot at **enemy**

##Enemy destroyed
